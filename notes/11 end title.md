## Scene 11: end title.sif

### Voiceover: 

_"Frag.. den.. Staat"_ (replace with the name/tagline of your own website)

### Scene Description:

  
1. the name of the website appears as the voiceover mentions each word
2. the words are pushed together into a URL
3. credits appear underneath

### NOTES:  
A very basic scene, but you will obviously have to change a great deal here as everything is specific to Frag den Staat, but perhaps the 'words-sliding-into-a-URL' animation can be reused (replace each word in the .sif file, rename the groups, but keep the animation properties of each group) 


### Changes for international contexts:  
Everything. Logo, URL, text, fonts, credits, colours...
/svg/11 end title/end title.svg has a basic template for credits. 

I would suggest using a CC-BY or CC-BY-SA license on your video, but it's up to you, you can make it (C) All Rights Reserved or any other license that you may prefer. 
The video's copyright can be assigned to whoever coordinated, adapted, assembled and exported your video, or to the organisation who funded the video's creation, or more than one person.

Due to the Creative Commons Attribution license I have used on this project, it is a legal requirement that any work made with this project includes the attribution:  
**'Based on 'Frag den Staat Animation Project' CC-BY Sam Muirhead (cameralibre.cc)'**  
This can be in your local language, or whatever language the rest of your credits are in.

If you used different music or other free cultural artworks in your video, make sure to attribute them here.


The sheer number of sound effects used in the video makes proper attribution difficult - in my original video I credited it like this:  

**SOUND FX  
CC-BY & CC0 from FreeSound.org  
Attribution and Links: cameralibre.cc/fragdenstaat**

This of course requires that http://cameralibre.cc/fragdenstaat will always maintain a list of attributions and hyperlinks to the original files on freesound.org, or at least cameralibre.cc/fragdenstaat will result in a redirection to a page with this list.
I did this because an unclickable list of almost 50 clips and sound artists is not particularly useful, or readily comprehensible. 
If anyone has a better suggestion for an elegant solution to this, please let me know.